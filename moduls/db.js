
const db = require('mysql2-promise')();
require('dotenv').config()

db.configure({
    "host": process.env.DB_HOST,
    "user": process.env.DB_USER,
    "password": process.env.DB_PASS,
    "database": process.env.db_name
});
module.exports = db;
