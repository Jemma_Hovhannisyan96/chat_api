'use strict'
const db = require('./db')


module.exports.getImages = function (){
    const query = 'Select * from products'

    return db.execute(query, []).spread(function (result) {
        return result;
    });
}


module.exports.getSoftProducts = function (){
    const query = 'Select * from soft_products'

    return db.execute(query, []).spread(function (result) {
        return result;
    });
}

